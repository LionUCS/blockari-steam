const inspect = require("util").inspect;

module.exports = eleventyConfig=>{
    eleventyConfig.addWatchTarget('./src/sass');
    eleventyConfig.addPassthroughCopy('./src/css');
    eleventyConfig.addPassthroughCopy('./src/img');
    eleventyConfig.addPassthroughCopy('./src/js');
    eleventyConfig.addPassthroughCopy("./src/favicon.svg");
    eleventyConfig.addFilter("debug", c=> `<pre>${inspect(c)}</pre>`);
    eleventyConfig.addFilter('stringify', JSON.stringify);
    eleventyConfig.addFilter('json2css', o=>o ? Object.keys(o).map(k=>`${k}: ${o[k]}`).join('; ') : null);

    return {
        dir: {
            input: "src",
            output: "public"
        }
    }
};
