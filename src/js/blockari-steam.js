const blockari = document.querySelector('#blockari');
const blCanvas = document.querySelector('#bl-canvas');
const blSide = document.querySelector('#bl-side');
const blText = document.querySelector('#bl-text');
const blBlock = document.querySelector('#bl-block');
const blCode = document.querySelector('#bl-code');
const blSize = document.querySelector('#bl-size');
let node

const htmlPat = [
    {pat: /<(b|strong)>(.+?)<\/\1>/, out: '[b]$2[/b]'},
    {pat: /<(i|em)>(.+?)<\/\1>/, out: '[i]$2[/i]'},
    {pat: /<(u|ins)>(.+?)<\/\1>/, out: '[u]$2[/u]'},
    {pat: /<a .*href="(.+?)".*>(.+?)<\/a>/, out: '[url=$1]$2[/url]'},
    {pat: /<(div)>(.+)?\n*?<\/\1>/, out: '$2'},
    {pat: /<br\/?>/, out: ''}
];
const bbText = el=>{
    let out = '';
    el.childNodes.forEach(e=>{
        let string = '';
        if(e.nodeType === Node.TEXT_NODE) string = e.textContent;
        else{
            string = e.outerHTML;
            htmlPat.forEach(m=>string = string.replace(m.pat, m.out));
        }
        out += string;
    });
    return out;
};
const bbElText = el=>{
    let out = '';
    Array.from(el.children).forEach(e=>{
        out += bbText(e);
    });
    return out;
};
const bbImg = el=>{
    if(!el.file) throw new Error('Missing image file');
    let out = `[img]{STEAM_APP_IMAGE}/extras/${el.file.name}[/img]`;
    if(el.dataset.href) out = `[url=${el.dataset.href}]${out}[/url]`;
    return out;
};
const bbFeatured = el=>{
    let out = '';
    Array.from(el.children).forEach(e=>{
        switch(e.nodeName){
            case 'BR':
                out += '\n';
                break;
            case 'IMG':
                out += bbImg(e);
                break;
            case 'STRONG':
                out += `[b]${bbText(e).trim()}[/b]`;
                break;
            case 'SPAN':
                out += bbText(e).trim();
                break;
        };
    });
    return out+'\n';
};
const bbList = el=>{
    let type = el.dataset.pip == 0 ? 'list' : 'olist';
    let string = `[${type}]\n`;
    Array.from(el.children).forEach(e=>{
        string += `[*]${bbText(e)}\n`;
    });
    return string+`[/${type}]`;
};
const toBB = ()=>{
    let out = '';
    try{
        Array.from(blCanvas.children).forEach((el, i)=>{
            let {type, id} = el.dataset;
            let string = i && (type !== 'element-img' || el.previousElementSibling.dataset.type !== 'element-img') ? '\n' : '';
            switch(type){
                case 'element-text':
                    string += bbElText(el);
                    break;
                case 'element-h2':
                    string += `[h2]${bbText(el).trim()}[/h2]`;
                    break;
                case 'element-img':
                    string += bbImg(el);
                    break;
                case 'embed-app':
                    if(!id) throw new Error('Missing Steam App ID');
                    string += `https://store.steampowered.com/app/${id}/`;
                    break;
                case 'embed-workshop':
                    if(!id) throw new Error('Missing Steam Workshop ID');
                    string += `https://steamcommunity.com/sharedfiles/filedetails/?id=${id}`;
                    break;
                case 'embed-youtube':
                    if(!id) throw new Error('Missing Youtube Video ID');
                    string += `https://www.youtube.com/watch?v=${id}`;
                    break;
                case 'feature-head':
                case 'feature-hero':
                case 'feature-line':
                case 'feature-sandwich':
                    string += bbFeatured(el);
                    break;
                case 'list-default':
                case 'list-ipip':
                case 'list-upip':
                    string += bbList(el);
                    break;
            };
            out += string;
        });
        return out;
    } catch(err){
        console.error(err);
        Swal.fire({
            title: 'Error!',
            text: err,
            icon: 'error',
            confirmButtonText: 'Okay'
        });
    }
};
const toFile = string=>{
    let blob = new Blob([string], {type: "text/plain;charset=utf-8"});
    let url = window.URL.createObjectURL(blob);
    let link = document.createElement("a");
    link.download = 'steam.bb.txt';
    link.href = url;
    link.click();
    window.URL.revokeObjectURL(url);
};
const addBlock = ({html})=>{
    let frag = document.createRange().createContextualFragment(html);
    blCanvas.appendChild(frag);
    blCanvas.lastElementChild.focus();
};
const selectFile = async format=>{
    let file = document.createElement('input');
    file.type = 'file';
    file.accept = format;
    file.click();
    return new Promise((resolve, reject)=>{
        file.onchange = e=>{
            node.file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = (img=>e=>img.src = e.target.result)(node);
            reader.readAsDataURL(node.file);
            resolve(node.file);
        };
    });
};
const canvasOnBlur = e=>{
    let r = e.relatedTarget;
    let p;
    if(r && r.parentNode) p = r.parentNode;
    if(!p || !p.classList.contains('bl-edit')) return;
    node = e.target;
};
const sideOnClick = e=>{
    if(e.target == e.currentTarget) return false;
    addBlock(e.target.dataset);
};
const promptObj = {
    'embed-app': {
        reg: /(?:steampowered\.com\/app\/)?(\d+)/,
        config: {
            title: 'Enter Steam App ID',
            inputPlaceholder: 'https://store.steampowered.com/app/[APP_ID]',
        },
        validationMessage: 'Enter Steam App ID or URL',
        src: id=>`https://store.steampowered.com/widget/${id}/`
    },
    'embed-workshop': {
        reg: /(?:steamcommunity\.com\/sharedfiles\/filedetails\/\?id=)?(\d+)/,
        config: {
            title: 'Enter Steam Workshop ID',
            inputPlaceholder: 'https://steamcommunity.com/sharedfiles/filedetails/?id=[WORKSHOP_ID]'
        },
        validationMessage: 'Enter Steam Workshop ID or URL',
        src: id=>`https://steamcommunity.com/sharedfiles/filedetails/?id=${id}`
    },
    'embed-youtube': {
        reg: /(?:youtu(?:\.be\/|be.com\/\S*(?:watch|embed)(?:(?:(?=\/[-a-zA-Z0-9_]{11,}(?!\S))\/)|(?:\S*v=|v\/))))?([\w-]{11,})/,
        config: {
            title: 'Enter Youtube ID',
            inputPlaceholder: 'https://www.youtube.com/watch?v=[YOUTUBE_ID]'
        },
        validationMessage: 'Enter Youtube ID or URL',
        src: id=>`https://www.youtube.com/embed/${id}?showinfo=0&autohide=1&fs=1&hd=1&modestbranding=1&rel=0&showsearch=0&wmode=direct&autoplay=0`
    }
};
const embedPrompt = async type=>{
    let {reg, config, validationMessage, src} = promptObj[type];
    let {value} = await Swal.fire({
        input: 'text',
        showCancelButton: true,
        returnFocus: false,
        preConfirm: input=>{
            let id = input.match(reg);
            if(!id) return Swal.showValidationMessage(validationMessage);
            return id[1];
        }, ...config
    });
    if(value){
        node.dataset.id = value;
        node.children[0].setAttribute('src', src(value));
    }
    return Promise.resolve(value);
};
let selection;
const getSel = ()=>{
    if(window.getSelection){
        let sel = window.getSelection();
        if(sel.getRangeAt && sel.rangeCount) selection = sel.getRangeAt(0);
    } else if(document.selection && document.selection.createRange) selection = document.selection.createRange();
};
const setSel = ()=>{
    if(!selection) return;
    if(window.getSelection){
        let sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(selection);
    } else if (document.selection && selection.select) selection.select();
};
const textOnClick = async e=>{
    if(!node || e.target == e.currentTarget) return false;
    let cmd = e.target.value;
    let {type} = node.dataset;
    switch(cmd){
        case 'embed':
            if(type.includes('embed-')) await embedPrompt(type);
            else if(type === 'element-img') await selectFile('image/png, image/webp, image/jpeg, image/gif');
            break;
        case 'list':
            if(type == 'list-default') node.dataset.pip ^= 1;
            break;
        case 'createLink':
            getSel();
            if(type.includes('embed-')) break;
            let input = await Swal.fire({
                title: 'Enter URL Address',
                input: 'url',
                inputPlaceholder: 'https://www.example.com',
                showCancelButton: true,
                showDenyButton: true,
                denyButtonText: 'Unlink',
                returnFocus: false
            });
            if(type === 'element-img'){
                if(input.isConfirmed){
                    node.dataset.href = input.value;
                    node.title = input.value;
                } else{
                    delete node.dataset.href;
                    node.title = '';
                }
            } else{
                setSel();
                if(input.isConfirmed){
                    document.execCommand(cmd, false, input.value);
                } else{
                    document.execCommand('unlink');
                }
            }
            break;
        default:
            document.execCommand(cmd);
    }
    node.focus();
    node = null;
};
const blockOnClick = e=>{
    if(!node || e.target == e.currentTarget) return false;
    let pr, nt;
    let el = node;
    if(el.parentNode.dataset.type && el.parentNode.dataset.type.includes('feature-')) el = el.parentNode;
    const pa = el.parentNode;
    switch(e.target.value){
        case 'delete':
            pa.removeChild(el);
            break;
        case 'up':
            pr = el.previousElementSibling;
            if(pr) pa.insertBefore(el, pr);
            break;
        case 'down':
            nt = el.nextElementSibling;
            if(!nt) return;
            nt = nt.nextElementSibling;
            if(nt) pa.insertBefore(el, nt);
            else pa.appendChild(el);
            break;
    }
    node.focus();
    node = null;
};
const listOnClick = e=>{
    if(!node || e.target == e.currentTarget) return false;
    let pa = node.parentNode;
    pa.dataset.pip ^= 1;
    node.focus();
    node = null;
};
const sizeOnClick = e=>{
    if(e.target == e.currentTarget) return false;
    blockari.classList = e.target.classList;
};
const codeOnClick = e=>{
    if(e.target == e.currentTarget) return false;
    const bb = toBB();
    if(!bb) return;
    Swal.fire({
        title: 'Save to BBCode File?',
        showCancelButton: true,
        confirmButtonText: 'Save',
        returnFocus: false
    }).then(result=>{
        if(!result.isConfirmed) return;
        console.log(bb);
        toFile(bb);
    });
};
blCanvas.addEventListener('focusout', canvasOnBlur, false);
blSide.addEventListener('click', sideOnClick, false);
blText.addEventListener('click', textOnClick, false);
blBlock.addEventListener('click', blockOnClick, false);
blSize.addEventListener('click', sizeOnClick, false);
blCode.addEventListener('click', codeOnClick, false);
